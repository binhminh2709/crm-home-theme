//anim
AUI().use('aui-base', 'aui-node', 'aui-io', 'aui-event', 'anim', function(A) {
	//search box animataion
	var a = new A.Anim({
		node : '#searchInput',
		duration : 0.1,
		easing : A.Easing.bounceOut
	});

	A.one('#righ-header-search').on('mouseenter', function(event) {
		var toArrat = {
			width : 300,
			opacity : 0.5
		};
		a.set("to", toArrat);
		a.run();
	});

	A.one('#righ-header-search').on('mouseleave', function(event) {
		var toArrat1 = {
			width : 1,
			opacity : 0.5
		};
		a.set("to", toArrat1);
		a.run();
		a.destroy();
	});

	A.one('#searchButton').on('click', function(event) {
		document.themeSearchForm.submit();
	});
});

AUI().ready(
	/* This function gets loaded when all the HTML, not including the portlets, is loaded. */
	function() { }
);

Liferay.Portlet.ready(
	/*
	This function gets loaded after each and every portlet on the page.
	portletId: the current portlet's id
	node: the Alloy Node object of the current portlet
	*/
	function(portletId, node) {}
);

Liferay.on(
	'allPortletsReady',
	/* This function gets loaded when everything, including the portlets, is on the page. */
	function() {}
);